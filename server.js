var http = require('http');
var fs = require('fs');
var url = require('url');

//Port
var port = 80;
//Home
var home = "/home.html";

// Init Server
 var server = http.createServer( function (request, response) {  
   var pathname = url.parse(request.url).pathname;
   
   // output filenames
   console.log("Request for " + pathname + " received.");
   if(pathname == "/"){
	   pathname = home;
   }   
   // get file contents
   fs.readFile(pathname.substr(1), function (err, data) {
      if (err) {
         console.log(err);
         // HTTP Status: 404 : NOT FOUND
         response.writeHead(404, {'Content-Type': 'text/html'});
      }else{	         
         // HTTP Status: 200 : OK
		 if(pathname.indexOf(".css") >0){ //css
			 response.writeHead(200, {'Content-Type': 'text/css'});
		 }else if(pathname.indexOf(".js") >0){ //js
			 response.writeHead(200, {'Content-Type': 'text/js'});
		 }else{ //Other, html
			response.writeHead(200, {'Content-Type': 'text/html'});
		 }
         //Response
         response.write(data.toString());		
      }
      response.end();
   });   
});
//Listen
server.listen(port);

// Content log
console.log('Server running at port:' +port);