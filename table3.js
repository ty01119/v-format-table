/**
 * @author William
 */
(function() {
	'use strict';
	window.Table3 = React.createClass({
	getInitialState:function(){
		var fileOptions = [[{label:"Nil", value:0}]];
		var tagOptions =  [[{label:"Nil", value:0}]];
		return {
			fileOptions:fileOptions,
			tagOptions:tagOptions
			};
	},
	componentWillMount:function(){
		//TODO AJAX, get data before mount
		//...
		//test!!!
		var fileOptions = [		    		    
		    [{label:"file1", value:1}, {label:"file2", value:2}, {label:"file3", value:3},{label:"file4", value:4},{label:"file5", value:5}],
		    [{label:"file6", value:6}, {label:"file7", value:7}, {label:"file8", value:8},{label:"file9", value:9},{label:"file10", value:10}],
		    [{label:"file11", value:11}, {label:"file12", value:12}, {label:"file13", value:13}]	
		    ];
		var tagOptions = [	    		    
		    [{label:"tag1", value:1}, {label:"tag2", value:2}, {label:"tag3", value:3}],
		    [{label:"tag4", value:4},{label:"tag5", value:5}],
		    [{label:"tag6", value:6}, {label:"tag7", value:7}], 
		    [{label:"tag8", value:8},{label:"tag9", value:9},{label:"tag10", value:10}],
		    [{label:"tag11", value:11}, {label:"tag12", value:12}, {label:"tag13", value:13}],
		    [{label:"tag14", value:14}]
		    ];
		this.setState({fileOptions:fileOptions, tagOptions:tagOptions});
	},
	fileSelected:function(value, checked){
		if(checked){
			alert(value+"Selected");
		}else{
			alert(value+"Unselected");
		}
	},
	tagSelected:function(value, checked){
		if(checked){
			alert(value+"Selected");
		}else{
			alert(value+"Unselected");
		}
	},
  render: function() {
    return (
    	<div>
    	
		<div className="row">
				<label className="col-md-1">single select file</label>
				<div className="col-md-6">
				<OptionTable options={this.state.fileOptions} isRadio={true}
								callback={this.fileSelected}/>
				</div>
		</div>
		<br/><br/>
		<div className="row">
				<label className="col-md-1">multi select tag</label>
				<div className="col-md-6">
				<OptionTable options={this.state.tagOptions} isRadio={false}
								callback={this.tagSelected}/>
				</div>
		</div>
		
		</div>
    );
  }
});

})();