/*
 * @auther: GreatJiang\Vanch
 * 分页组件
*/
(function (){
	'use strict';
	//分页
	window.Pages = React.createClass({
		propTypes:{
			totalNum:React.PropTypes.number,
			numPerPage:React.PropTypes.number,
			start:React.PropTypes.number,
			curPage:React.PropTypes.number,
			pageAction:React.PropTypes.func.isRequired
		},
		getDefaultProps:function(){
			return {
				totalNum:0,
				numPerPage:20,
				start:0
			}
		},
		getInitialState:function (){
			var totalPage=Math.floor((Number(this.props.totalNum) +Number(this.props.numPerPage)-1)/Number(this.props.numPerPage));
			var curPage, start;
			if(this.props.curPage >0){
				curPage = Number(this.props.curPage);
				start = (curPage-1)*Number(this.props.numPerPage);
			}else{
				start = this.props.start;
				curPage = Math.floor(start/Number(this.props.numPerPage)) + 1;	
			}
			return ({
				curPage:curPage,
				totalPage:totalPage
			}); 
		},
		prev:function (){
			//获取当前page
			var curPage = this.state.curPage;
			if(curPage >1){
				this.props.pageAction({
					start:(curPage -1 -1)*this.props.numPerPage,
					numPerPage:this.props.numPerPage
				});
			}else{
				alert('已经是第一页！');
			}
		},
		next:function (){
			//获取当前page
			var curPage = this.state.curPage;
			var totalPage = this.state.totalPage;
			if(curPage < totalPage){
				this.props.pageAction({
					start:curPage*this.props.numPerPage,
					numPerPage:this.props.numPerPage
				});
			}else{
				alert('已经是最后一页！');
			}
		},
		search:function (e){
			var self=this;
			if(e.keyCode != 13) return false;
			//获取当前page
			var totalPage=this.state.totalPage;
			
			if(isNaN(destPage) || destPage <= 0 || destPage === ''){
				alert('搜索页码必须是大于0的正整数数字！');
				return false;
			}
			if(destPage>totalPage){
				alert('搜索页码不能大于最大页码！');
				return false;
			}else{
				this.props.pageAction({
					start:(Number(destPage)-1)*this.props.numPerPage,
					numPerPage:this.props.numPerPage
				});
			}
		},
		firstPage:function (){
			this.refs.destPage.value = 1;
			this.props.pageAction({
				start:0,
				numPerPage:this.props.numPerPage
			});
		},
		lastPage:function (){
			var totalPage=this.state.totalPage;
			this.props.pageAction({
				start:(totalPage-1)*this.props.numPerPage,
				numPerPage:this.props.numPerPage
			});
		},
		componentWillReceiveProps:function(nextProps){
			var totalNum, numPerPage;
			var curPage=this.state.curPage;
			var start;
			if(nextProps.totalNum){
				totalNum = Number(nextProps.totalNum); 
			}else{
				totalNum = this.props.totalNum;
			}
			
			if(nextProps.numPerPage){
				numPerPage = Number(nextProps.numPerPage); 
			}else{
				numPerPage = this.props.numPerPage;
			}
			
			if(nextProps.curPage !== undefined){
				curPage = Number(nextProps.curPage);
				start = (curPage-1)*numPerPage;
			}else if(nextProps.start !== undefined){
				start = Number(nextProps.start);
				curPage = Math.floor(start/numPerPage) +1;
			}
			//alert(nextProps.curPage +", " +nextProps.start);//test!!!
			var totalPage = Math.floor((totalNum +numPerPage -1)/numPerPage);
			this.refs.destPage.value = curPage;
			this.setState({
				totalPage:totalPage,
				curPage:curPage
			});
		},
		render : function (){
			return (
				<div style={{width:'100%',height:'34px'}}>
					<ul className="pagination pull-right" style={{margin:0,marginTop:'-10px'}}>
						<li>
							<a href="javascript:;" onClick={this.firstPage}>首页</a>
						</li>
						<li>
							<a href="javascript:;" onClick={this.prev}>上一页</a>
						</li>
						<li>
							<a style={{'borderTop':'hidden','borderBottom':'hidden'}}> <input ref="destPage" type="number" style={{width:"40px", textAlign:"center"}} defaultValue={this.state.curPage}></input> / {this.state.totalPage} </a> 
						</li>
						<li>
							<a href="javascript:;" onClick={this.next}>下一页</a>
						</li>
						<li>
							<a href="javascript:;" onClick={this.lastPage}>尾页</a>
						</li>
					</ul>
				</div>
			);
		}
	});
})()

