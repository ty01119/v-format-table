/**
 * @author William
 */
(function() {
	'use strict';
	window.Table2 = React.createClass({
	getInitialState:function(){
		var data = [];
		var tds = [{format:this.formatCheckCol}, {col:"id"}, {col:"name"}, {col:"src", format:this.formatSrc}, 
			{col:"date"}, {format:this.formatOperations, has_td:true}];
	    var ths = [{format:this.formatCheckTH}, {colName:"ID"}, {colName:"Name"}, {colName:"Src"}, 
	    	{colName:"Rigister Date"}, {colName:"Action"}];
	    var ops = [{opid:1,name:"Edit",callback:this.handleClick},{opid:2,name:"Delete",callback:this.handleClick}];
		return {
			start:0,
			data:data,
			ths:ths,
			tds:tds,			
			ops:ops};
	},
	componentWillMount:function(){
		//TODO AJAX, get data before mount
		//...
		
		//test!!!
//		var ajaxdata = [		    		    
//		    {id:1, name:'丁一', date:'2016-08-11', src:'1'},
//				{id:2, name:'不二', date:'2016-08-10', src:'2'},
//		    		{id:3, name:'张三', date:'2016-08-09', src:'1'}
//		    		];
//		this.setState({data:ajaxdata});
	},
	componentDidMount:function(){
		//TODO AJAX, get data after mount
		//...
		
		//test!!!
		var totalNum = 80;
		var numPerPage = 10;
		var ajaxdata = [		    
							{id:1, name:'Harry', date:'2016-08-11', src:'2'},
							{id:2, name:'Mandy', date:'2016-08-10', src:'2'},
							{id:3, name:'Abby', date:'2016-08-09', src:'1'},
							{id:4, name:'Bill', date:'2016-08-12', src:'1'},
							{id:5, name:'Cara', date:'2016-08-13', src:'2'},
							{id:6, name:'Daisy', date:'2016-08-14', src:'2'},
							{id:7, name:'Edward', date:'2016-08-14', src:'2'},
							{id:8, name:'Fiona', date:'2016-08-14', src:'2'},
							{id:9, name:'Gary', date:'2016-08-14', src:'1'},
							{id:10, name:'Jack', date:'2016-08-14', src:'2'}
		    		];
		this.setState({totalNum:totalNum, numPerPage:numPerPage, data:ajaxdata});    		
	},
	ajaxGetData:function(start, num){
		//TODO AJAX, get data
		//..		
		
		//test data!!!
		var ajaxdata = [
					{id:start+1, name:'Harry'+(start+1), date:'2016-08-12', src:'1'},
					{id:start+2, name:'Mandy'+(start+2), date:'2016-08-13', src:'2'},
					{id:start+3, name:'Abby' +(start+3), date:'2016-08-14', src:'2'},
					{id:start+4, name:'Bill'+(start+4), date:'2016-08-12', src:'1'},
					{id:start+5, name:'Cara'+(start+5), date:'2016-08-13', src:'2'},
					{id:start+6, name:'Daisy' +(start+6), date:'2016-08-13', src:'2'},
					{id:start+7, name:'Edward' +(start+7), date:'2016-08-14', src:'2'},
					{id:start+8, name:'Fiona' +(start+8), date:'2016-08-12', src:'2'},
					{id:start+9, name:'Gary'  +(start+9), date:'2016-08-13', src:'1'},
					{id:start+10, name:'Jack'  +(start+10), date:'2016-08-16', src:'2'}
		    		];
		    		
		//refresh with the new data.
		this.refreshData(start, ajaxdata);
	},
	refreshData:function(start, data){
		this.setState({start:start, data:data});
	},
	handleTHCheckStatusChange:function(e){
		for(var i=0; i<this.state.data.length; i++){			
			this.state.data[i]['checked'] = e.target.checked;	
		}
		this.setState({data:this.state.data});
	},
	handleCheckStatusChange:function(e){
		var id = Number(e.target.name);		
//		alert("id=" +id +", checked=" +e.target.checked);
		this.state.data[id]['checked'] = e.target.checked;
		this.setState({data:this.state.data});
	},
	formatCheckTH:function(index){//全选
		return (<label><input type="checkbox" name={index} onChange={this.handleTHCheckStatusChange} checked={this.state.checkAll}/></label>);
	},
	formatCheckCol:function(data,checkIndex){//单选
		return (<label><input type="checkbox" name={checkIndex} onChange={this.handleCheckStatusChange} checked={this.state.data[checkIndex]['checked']} value={this.state.data[checkIndex]['checked']}/></label>);
	},
	formatSrc:function(data, index){
		var srcs = ["Unknown","Rigisted From","Suggested Rigister"];
		return srcs[data.src];
	},
	formatOperations:function(data, index){
		var ops = this.state.ops;
		return (<CommonTDOperations key="-2" ops={ops} data={index}/>);
	},
	viewOp:function(index){
		//TODO the view operation.
		alert("viewOp:" +JSON.stringify(this.state.data[index]));
	},
	delOp:function(index){
		//TODO the view operation.
		this.state.data.baoremove(index);
		this.setState({data:this.state.data});
	},
	handleClick:function(opid, index){
		var row_id = index;
		if(opid == 1){
			this.viewOp(index);
		}else if(opid == 2){
			this.delOp(index);
		}
	},
	multiDelete:function(){
		var data = this.state.data;
		var newdata = [];
		var length = data.length;
		for(var i=0; i<length; i++){			
			if(!data[i]['checked']){
				newdata.push(data[i]);
			}
		}
		this.setState({data:newdata});
	},
	pageAction:function(params){
		this.ajaxGetData(params.start, params.numPerPage);
	},
  render: function() {
    return (
		<div>			
			
			<div className="dataTable_wrapper">				
				<input className="btn btn-primary" type="button" value="Delete Selected" onClick={this.multiDelete}/>					
				<br/><br/>
			
				<FormatTable ths={this.state.ths} data={this.state.data} tds={this.state.tds}/>
				
				<div>
					<Pages  numPerPage={this.state.numPerPage} totalNum={this.state.totalNum} start={this.state.start} pageAction={this.pageAction}/>
				</div>
			</div>
         </div>
    );
  }
});

})();